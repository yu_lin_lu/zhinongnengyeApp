import Vue from 'vue'
import App from './App'
import zaiLattice from "./components/cmd-avatar/cmd-avatar.vue"
import axios from 'util/axios.js'

Vue.config.productionTip = false
Vue.prototype.$axios = axios
//配置根路径
axios.defaults.baseURL = 'http://localhost:8083/'
Vue.prototype.$http = axios


App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
